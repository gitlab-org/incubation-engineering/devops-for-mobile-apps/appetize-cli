## [Unreleased]

## [0.1.4] - 2021-09-11

- Upload command will detect & update existing Review Apps when running in GitLab CI

## [0.1.3] - 2021-09-09

- Upload command now returns full JSON response from Appetize

## [0.1.2] - 2021-09-02

- Adding specs, cleaning up gemspec, updated README

## [0.1.1] - 2021-08-30

- Initial release
