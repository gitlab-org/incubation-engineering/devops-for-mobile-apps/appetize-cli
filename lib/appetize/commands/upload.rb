# frozen_string_literal: true

require_relative "../command"
require_relative "../api"
require_relative "../ci/gitlab"

module Appetize
  module Commands
    class Upload < Appetize::Command
      def initialize(path, platform, token, api_host, options)
        @api      = Appetize::API.new(token, api_host)
        @path     = path
        @platform = platform
        @options  = options
      end

      def execute(input: $stdin, output: $stdout)
        # If we are running in GitLab CI, check for an existing pipeline
        # if a pipeline is found, fetch the public key and update the
        # existing Appetize app
        if Appetize::Commands::Upload.gitlab_ci?
          artifact_response = fetch_gitlab_artifact

          if artifact_response.code == 200
            public_key = JSON.parse(artifact_response.body)["publicKey"]

            response = @api.update(@path, public_key)
          end
        end

        response = @api.create(@path, @platform) if public_key.nil?

        raise "Upload failed: #{response.code}" unless response.code == 200

        output.puts response.body
      end

      def self.gitlab_ci?
        ENV["GITLAB_CI"].to_s.downcase == "true"
      end

      private

      def fetch_gitlab_artifact
        gitlab_client = Appetize::CI::GitLab.new(
          ENV["CI_PROJECT_ID"],
          ENV["CI_COMMIT_REF_NAME"],
          ENV["PRIVATE_TOKEN"],
          ENV["CI_API_V4_URL"]
        )

        gitlab_client.fetch_artifact(
          "startReview",
          "appetize-information.json"
        )
      end
    end
  end
end
