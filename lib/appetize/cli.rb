# frozen_string_literal: true

require "thor"

module Appetize
  # Handle the application command line parsing
  # and the dispatch to various command objects
  #
  # @api public
  class CLI < Thor
    # Error raised by this runner
    Error = Class.new(StandardError)

    desc "version", "appetize version"
    def version
      require_relative "version"
      puts "v#{Appetize::VERSION}"
    end
    map %w[--version -v] => :version

    desc "update PATH PUBLIC_KEY", "Command description..."
    method_option :help, aliases: "-h", type: :boolean,
                         desc: "Display usage information"
    def update(path, public_key, token = nil, api_host = nil)
      if options[:help]
        invoke :help, ["update"]
      else
        require_relative "commands/update"
        Appetize::Commands::Update.new(path, public_key, token, api_host, options).execute
      end
    end

    desc "upload PATH PLATFORM", "Upload an APK/ZIP to Appetize"
    method_option :help, aliases: "-h", type: :boolean,
                         desc: "Display usage information"
    def upload(path, platform, token = nil, api_host = nil)
      if options[:help]
        invoke :help, ["upload"]
      else
        require_relative "commands/upload"
        Appetize::Commands::Upload.new(path, platform, token, api_host, options).execute
      end
    end

    desc "delete PUBLIC_KEY", "Delete an app deploymment from Appetize"
    method_option :help, aliases: "-h", type: :boolean,
                         desc: "Display usage information"
    def delete(public_key, token = nil, api_host = nil)
      if options[:help]
        invoke :help, ["delete"]
      else
        require_relative "commands/delete"
        Appetize::Commands::Delete.new(public_key, token, api_host, options).execute
      end
    end
  end
end
