# Appetize CLI

The Appetize CLI provides a command line interface to the [Appetize API](https://docs.appetize.io/api/overview). 

## Installation

Install this gem by executing:

    $ gem install appetize-cli

## Usage

Currently there are two supported actions `upload` and `delete` - `help` and `version` commands are also supplied.

```
Commands:
  appetize delete PUBLIC_KEY     # Delete an app deploymment from Appetize
  appetize help [COMMAND]        # Describe available commands or one specific command
  appetize upload PATH PLATFORM  # Upload an APK/ZIP to Appetize
  appetize version               # appetize version
```

### Setup

All actions in this tool require an Appetize API token, which can be generated in your Appetize account. Once a token has been generated you must set it as the environment variable `APPETIZE_API_TOKEN`. 

Example:

	$ export APPETIZE_API_TOKEN=yourtoken


### Upload

Upload an APK/ZIP to Appetize

```
Usage:
  appetize upload PATH PLATFORM
```

`PATH` the path to the Apk or Zip file of the app you would like to deploy

`PLATFORM` either `ios` or `android`

Example:

	$ appetize upload builds/app-debug.apk android
	$ appetize upload builds/app.zip ios

### Update

Update an existing app in Appitize with a new APK/ZIP

```
Usage:
  appetize update PATH PUBLIC_KEY
```

`PATH` the path to the Apk or Zip file of the app you would like to deploy

`PUBLIC_KEY` the public key provided by Appetize for the specific app deployment you'd like to update

Example:

	$ appetize upload builds/app-debug.apk somepublickey
	$ appetize upload builds/app.zip somepublickey


### Delete

Delete an app deploymment from Appetize

```
Usage:
  appetize delete PUBLIC_KEY
```

`PUBLIC_KEY` the public key provided by Appetize for the specific app deployment you'd like to delete

Example:

	$ appetize delete somepublickey
	

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bundle exec rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/appetize-cli/-/issues. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/appetize-cli/-/blob/master/CODE_OF_CONDUCT.md).

## Code of Conduct

Everyone interacting in the Appetize CLI project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/appetize-cli/-/blob/master/CODE_OF_CONDUCT.md).

## Copyright

Copyright (c) 2021 GitLab, Inc. See [MIT License](LICENSE.txt) for further details.