# frozen_string_literal: true

RSpec.describe "`appetize update` command", type: :cli do
  it "executes `appetize help update` command successfully" do
    output = `appetize help update`
    expected_output = <<~OUT
      Usage:
        appetize update PATH PUBLIC_KEY

      Options:
        -h, [--help], [--no-help]  # Display usage information

      Command description...
    OUT

    expect(output).to eq(expected_output)
  end
end
