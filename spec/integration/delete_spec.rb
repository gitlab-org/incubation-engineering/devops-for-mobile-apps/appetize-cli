# frozen_string_literal: true

RSpec.describe "`appetize delete` command", type: :cli do
  it "executes `appetize help delete` command successfully" do
    output = `appetize help delete`
    expected_output = <<~OUT
      Usage:
        appetize delete PUBLIC_KEY

      Options:
        -h, [--help], [--no-help]  # Display usage information

      Delete an app deploymment from Appetize
    OUT

    expect(output).to eq(expected_output)
  end
end
