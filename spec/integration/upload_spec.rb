# frozen_string_literal: true

RSpec.describe "`appetize upload` command", type: :cli do
  it "executes `appetize help upload` command successfully" do
    output = `appetize help upload`
    expected_output = <<~OUT
      Usage:
        appetize upload PATH PLATFORM

      Options:
        -h, [--help], [--no-help]  # Display usage information

      Upload an APK/ZIP to Appetize
    OUT

    expect(output).to eq(expected_output)
  end
end
