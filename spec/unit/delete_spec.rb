# frozen_string_literal: true

require "appetize/commands/delete"

RSpec.describe Appetize::Commands::Delete do
  it "executes `delete` command successfully" do
    expect(HTTParty).to receive(:delete).and_return(OpenStruct.new(code: 200, body: "OK"))

    output = StringIO.new
    public_key = nil
    options = {}
    command = Appetize::Commands::Delete.new(public_key, "fake_token", options)

    command.execute(output: output)

    expect(output.string).to eq("OK\n")
  end

  it "returns an error if the `delete` command fails with a 404" do
    expect(HTTParty).to receive(:delete).and_return(Net::HTTPNotFound.new("1.1", 404, ""))

    output = StringIO.new
    public_key = nil
    options = {}
    command = Appetize::Commands::Delete.new(public_key, "fake_token", options)

    expect { command.execute(output: output) }.to raise_error(RuntimeError, "Delete failed: 404")
  end

  it "returns an error if the `delete` command fails with a 401" do
    expect(HTTParty).to receive(:delete).and_return(Net::HTTPUnauthorized.new("1.1", 401, ""))

    output = StringIO.new
    public_key = nil
    options = {}
    command = Appetize::Commands::Delete.new(public_key, "fake_token", options)

    expect { command.execute(output: output) }.to raise_error(RuntimeError, "Delete failed: 401")
  end
end
