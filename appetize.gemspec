# frozen_string_literal: true

require_relative "lib/appetize/version"

Gem::Specification.new do |spec|
  spec.name          = "appetize-cli"
  spec.license       = "MIT"
  spec.version       = Appetize::VERSION
  spec.authors       = ["Darby Frey"]
  spec.email         = ["dfrey@gitlab.com"]

  spec.summary       = "A Command Line Interface to the Appetize.io API"
  spec.description   = "A Command Line Interface to the Appetize.io API"
  spec.homepage      = "https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/appetize-cli"
  spec.required_ruby_version = ">= 2.5.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/appetize-cli"
  spec.metadata["changelog_uri"] = "https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/appetize-cli/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  spec.add_dependency "httparty"
  spec.add_dependency "thor"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
